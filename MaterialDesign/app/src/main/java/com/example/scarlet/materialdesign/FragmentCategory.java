package com.example.scarlet.materialdesign;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class FragmentCategory extends Fragment {
    private String TAG = MainActivity.class.getSimpleName();
    //http://api.androidhive.info/json/glide.json
    private static final String endpoint = "http://api.androidhive.info/json/glide.json";
    private ArrayList<Image> images;
    //    private ProgressDialog pDialog;
    private ShimmerRecyclerView shimmerRecycler;
    private GalleryAdapter mAdapter;
    private RecyclerView recyclerView;

    public FragmentCategory() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_goods, null);
        Log.d("mylog", "part1");
//        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
//        RecyclerView.LayoutManager layoutManager;
//
//        getActivity().setTheme(R.style.AppTheme);
//        getActivity().setContentView(R.layout.activity_list);
       // layoutManager = new LinearLayoutManager(this.getContext());
      //  shimmerRecycler = (ShimmerRecyclerView) v.findViewById(R.id.shimmer_recycler_view);
        Log.d("mylog", "part2");

//        pDialog = new ProgressDialog(this);
//        images = new ArrayList<>();
//        mAdapter = new GalleryAdapter(getContext().getApplicationContext(), images);
//
//        recyclerView.setAdapter(mAdapter);
//
//        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext().getApplicationContext(), 1);
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        shimmerRecycler.setLayoutManager(layoutManager);
//        shimmerRecycler.setAdapter(mAdapter);
//        shimmerRecycler.showShimmerAdapter();
//        Log.d("mylog", "part3");
//        shimmerRecycler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                // loadCards();
//                fetchImages();
//                //
//            }
//        }, 2000);
//
////        setContentView(R.layout.gallery_thumbnail);
//        shimmerRecycler.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getActivity().getApplicationContext(), recyclerView, new GalleryAdapter.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("images", images);
//                bundle.putInt("position", position);
//
//                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
//                newFragment.setArguments(bundle);
//                newFragment.show(ft, "slideshow");
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));
//
//        Log.d("mylog", "part4");
//
//        recyclerView.setAdapter(mAdapter);
//        shimmerRecycler.showShimmerAdapter();

        return v;
    }


//    private void fetchImages() {
//        Log.d("mylog", "ok2");
////        pDialog.setMessage("Downloading json...");
////        pDialog.show();
//
//        JsonArrayRequest req = new JsonArrayRequest(endpoint,
//                new Response.Listener<JSONArray>() {
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        Log.d(TAG, response.toString());
//                        shimmerRecycler.hideShimmerAdapter();
////                        pDialog.hide();
//
//                        images.clear();
//                        for (int i = 0; i < response.length(); i++) {
//                            try {
//                                JSONObject object = response.getJSONObject(i);
//                                Image image = new Image();
//                                image.setName(object.getString("name"));
//
//                                JSONObject url = object.getJSONObject("url");
//                                image.setSmall(url.getString("small"));
//                                image.setMedium(url.getString("medium"));
//                                image.setLarge(url.getString("large"));
//                                image.setTimestamp(object.getString("timestamp"));
//
//                                images.add(image);
//
//                            } catch (JSONException e) {
//                                Log.e(TAG, "Json parsing error: " + e.getMessage());
//                            }
//                        }
////                        setContentView(R.layout.gallery_thumbnail);
//                        mAdapter.notifyDataSetChanged();
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Error: " + error.getMessage());
////                pDialog.hide();
//                shimmerRecycler.hideShimmerAdapter();
//            }
//        });
//
//        // Adding request to request queue
//        AppController.getInstance().addToRequestQueue(req);
//        Log.d("mylog","part0");
//    }





    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onActivityCreated( Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    }
